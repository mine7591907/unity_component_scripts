using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnParticleSystemStop : MonoBehaviour
{
    void Start()
    {
        var main = GetComponent<ParticleSystem>().main;
        main.stopAction = ParticleSystemStopAction.Callback;
    }

    private void OnParticleSystemStopped()
    {
        gameObject.SetActive(false);
    }
}
