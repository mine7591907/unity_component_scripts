using UnityEngine;
using UnityEngine.Events;

using System;
using System.Collections;

[Serializable]
public class UnityEventListener : UnityEvent { }
