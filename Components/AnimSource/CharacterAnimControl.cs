using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static DictionaryExtension;
using Sirenix.OdinInspector;

public class CharacterAnimControl : SerializedMonoBehaviour
{
    readonly static Dictionary<State, string> animNameDict = new Dictionary<State, string>()
    {
        {State.Idle, Enum.GetName(typeof(State), State.Idle) },
        {State.Die, Enum.GetName(typeof(State), State.Die) },
        {State.Walking, Enum.GetName(typeof(State), State.Walking) },
        {State.Jogging, Enum.GetName(typeof(State), State.Jogging) },
        {State.Running, Enum.GetName(typeof(State), State.Running) },
        {State.Block, Enum.GetName(typeof(State), State.Block) },
        {State.BlockHitImpact, Enum.GetName(typeof(State), State.BlockHitImpact) },
        {State.NormalAttack, Enum.GetName(typeof(State), State.NormalAttack) },
        {State.NormalAttack1, Enum.GetName(typeof(State), State.NormalAttack1) },
        {State.NormalAttack2, Enum.GetName(typeof(State), State.NormalAttack2) },
        {State.NormalAttack3, Enum.GetName(typeof(State), State.NormalAttack3) },
        {State.JogToStop, Enum.GetName(typeof(State), State.JogToStop) },
        {State.Jump,  Enum.GetName(typeof(State), State.Jump) },
        {State.Fall,  Enum.GetName(typeof(State), State.Fall) },
        {State.Dash,  Enum.GetName(typeof(State), State.Dash) },
        {State.Roll,  Enum.GetName(typeof(State), State.Roll) },
        {State.SheathWeapon,  Enum.GetName(typeof(State), State.SheathWeapon) },
        {State.GetHit,  Enum.GetName(typeof(State), State.GetHit) },
        {State.Parry,  Enum.GetName(typeof(State), State.Parry) },
        {State.BeingParried,  Enum.GetName(typeof(State), State.BeingParried) },

    };
    public readonly static State[] normalAttackComboList = new State[4]
    {
            State.NormalAttack, State.NormalAttack1, State.NormalAttack2 , State.NormalAttack3
    };


    [SerializeField] Animator animator;
    public Animator GetAnimator() { return animator; }
    [Header("Debug")]
    [ShowInInspector] Dictionary<State, AnimationClip> currentClips = new Dictionary<State, AnimationClip>();

    string currentAnim;

    private void Awake()
    {
        if (GetComponent<Animator>() != null)
        {
            animator = GetComponent<Animator>();
        }
        foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips)
        {
            if (animNameDict.ContainsValue(clip.name) == false) continue;
            currentClips[(State)Enum.Parse(typeof(State), clip.name)] = clip;
        }
    }

    public void PlayAnimCrossFade(State _state, float _normalizedFade = 0.1f, bool _isForcePlaying = false)
    {
        if(_isForcePlaying == false) if (currentAnim == animNameDict[_state]) return;
        animator.CrossFade(animNameDict[_state], _normalizedFade, 0, 0);
        currentAnim = animNameDict[_state];
    }

    public void PlayAnim(State _state, bool _isForcePlaying = false)
    {
        if (_isForcePlaying == false) if (currentAnim == animNameDict[_state]) return;
        animator.Play(animNameDict[_state],0 ,0);
        currentAnim = animNameDict[_state];
    }

    const string blendX = "BlendX";
    const string blendY = "BlendY";

    public void SetBlend(float _blendX, float _blendY)
    {
        animator.SetFloat(blendX, _blendX);
        animator.SetFloat(blendY, _blendY);
    }
    public float GetCurrentAnimLength()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length;
    }

    public float GetAnimLength(State _anim)
    {
        return currentClips[_anim].length;
    }


    public void SetOverrideAnims(AnimatorOverrideController _override)
    {
        animator.runtimeAnimatorController = _override;
        foreach (AnimationClipPair pairs in _override.clips)
        {
            if (pairs.overrideClip != null)
            {
                if (animNameDict.ContainsValue(pairs.overrideClip.name) == false) continue;
                currentClips[(State)Enum.Parse(typeof(State), pairs.overrideClip.name)] = pairs.overrideClip;
            }
            else
            {
                if (animNameDict.ContainsValue(pairs.originalClip.name) == false) continue;
                currentClips[(State)Enum.Parse(typeof(State), pairs.originalClip.name)] = pairs.originalClip;
            }
        }
    }



}
