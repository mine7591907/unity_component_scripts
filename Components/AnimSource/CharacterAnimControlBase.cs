using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimControlBase<ANIM_ENUM> : SerializedMonoBehaviour
{
    protected readonly static Dictionary<ANIM_ENUM, string> animNameDict = new Dictionary<ANIM_ENUM, string>();

    [SerializeField] Animator animator;
    public Animator GetAnimator() { return animator; }
    [Header("Debug")]
    [ShowInInspector] Dictionary<ANIM_ENUM, AnimationClip> currentClips = new Dictionary<ANIM_ENUM, AnimationClip>();

    string currentAnim;

    protected virtual void Awake()
    {
        if (GetComponent<Animator>() != null)
        {
            animator = GetComponent<Animator>();
        }
        foreach (var item in Enum.GetNames(typeof(ANIM_ENUM)))
        {
            animNameDict[(ANIM_ENUM)Enum.Parse(typeof(ANIM_ENUM), item)] = item;
        }
        foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips)
        {
            if (animNameDict.ContainsValue(clip.name) == false) continue;
            currentClips[(ANIM_ENUM)Enum.Parse(typeof(ANIM_ENUM), clip.name)] = clip;
        }
    }

    public void PlayAnimCrossFade(ANIM_ENUM _state, float _normalizedFade = 0.1f, bool _isForcePlaying = false, int layer = 0)
    {
        if (_isForcePlaying == false) if (currentAnim == animNameDict[_state]) return;
        animator.CrossFade(animNameDict[_state], _normalizedFade, layer, 0);
        currentAnim = animNameDict[_state];
    }

    public void PlayAnim(ANIM_ENUM _state, bool _isForcePlaying = false, int layer = 0)
    {
        if (_isForcePlaying == false) if (currentAnim == animNameDict[_state]) return;
        animator.Play(animNameDict[_state], layer, 0);
        currentAnim = animNameDict[_state];
    }

    const string blendX = "BlendX";
    const string blendY = "BlendY";

    public void SetBlend(float _blendX, float _blendY)
    {
        animator.SetFloat(blendX, _blendX);
        animator.SetFloat(blendY, _blendY);
    }
    public float GetCurrentAnimLength()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length;
    }

    public float GetAnimLength(ANIM_ENUM _anim)
    {
        return currentClips[_anim].length;
    }


    public void SetOverrideAnims(AnimatorOverrideController _override)
    {
        animator.runtimeAnimatorController = _override;
        foreach (AnimationClipPair pairs in _override.clips)
        {
            if (pairs.overrideClip != null)
            {
                if (animNameDict.ContainsValue(pairs.overrideClip.name) == false) continue;
                currentClips[(ANIM_ENUM)Enum.Parse(typeof(ANIM_ENUM), pairs.overrideClip.name)] = pairs.overrideClip;
            }
            else
            {
                if (animNameDict.ContainsValue(pairs.originalClip.name) == false) continue;
                currentClips[(ANIM_ENUM)Enum.Parse(typeof(ANIM_ENUM), pairs.originalClip.name)] = pairs.originalClip;
            }
        }
    }






}
