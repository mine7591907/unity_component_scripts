﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Sirenix.OdinInspector;

public class PatternController : MonoBehaviour
{
    [ListDrawerSettings(ShowIndexLabels = true)]
    public CommandProcessor[] patterns;

    int patternIndex = 0;
    bool inPattern = false;

    public void DoPattern()
    {
        inPattern = false;
        patterns[patternIndex].ExecuteSequencely(() => inPattern = true); ;
        NextIndex();
    }

    public bool IsInPattern() => inPattern;

    void NextIndex()
    {
        patternIndex++;
        if (patternIndex == patterns.Length)
        {
            patternIndex = 0;
        }
    }


}

