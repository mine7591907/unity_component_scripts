using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

public class Global 
{
    public const float gravity = -9.81f;
    public const float gravityFallMulti = 2.25f;
    public const float meleeAttackRange = 3f;
    public const float pushBackForce = 5;


}

public static class IEnumeratorExtension
{
    public static IEnumerator IEWaitTime(float _duration, Action _callback)
    {
        yield return new WaitForSeconds(_duration);
        _callback?.Invoke();
    }

    /// <summary>
    /// Don't use it to pass param with bool var
    /// </summary>
    /// <param name="_ok"></param>
    /// <param name="_callback"></param>
    /// <returns></returns>
    public static IEnumerator IEWaitTillOk(bool _ok, Action _callback)
    {
        yield return new WaitUntil(() => _ok);
        yield return null;
        _callback?.Invoke();
    }



}


public static class DictionaryExtension
{
    public static TKey GetKeyFromValue<TKey, TValue>(this Dictionary<TKey, TValue> _dictionary, TValue _targetValue)
    {
        foreach (var pair in _dictionary)
        {
            if (EqualityComparer<TValue>.Default.Equals(pair.Value, _targetValue))
            {
                return pair.Key;
            }
        }
        // If the value is not found, you may handle it accordingly (e.g., return default TKey or throw an exception)
        return default(TKey);
    }


}

public static class ColliderExtension
{
    public static IEnumerator IETurnOnCollider(this Collider _collider, float _timer)
    {
        yield return new WaitForSeconds(_timer);
        _collider.enabled = true;
    }

    public static IEnumerator IETurnOffCollider(this Collider _collider, float _timer)
    {
        yield return new WaitForSeconds(_timer);
        _collider.enabled = false;
    }

}

public static class GameObjectExtension
{
    public static void ChangeLayer(this GameObject _object, string _layer)
    {
        _object.layer = LayerMask.NameToLayer(_layer);
    }
}
