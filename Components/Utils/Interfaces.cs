
using System;
using UnityEngine;

public interface ICharacterGetHit
{
    public bool CantBeHit {  get; set; } 

    public void GetHit(int _input, Transform _giver, Action<HitStatus> _callbackHitStatus);
}

public enum HitStatus
{
    ActualHit, Parry, Block
}

public interface ICharacterAnimControl
{
    public CharacterAnimControl GetAnimControl();
}

public interface ICharacterStats
{
    public CharacterStatsControl GetStats();
}


public interface ICharacterDie
{
    public void Die();
}



