﻿using System.Collections;
using UnityEngine;


public class PlayerPrefKeys
{
    public const string musicVolumeNormalized = "music_volume";
    public const string sfxVolumeNormalized = "sfx_volume";
    public const string masterVolumeNormalized = "master_volume";

}
