using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class GlobalString 
{
    public const string axisHorizontal = "Horizontal";
    public const string axisVertical = "Vertical";
    public const string axisMouseX = "Mouse X";
    public const string axisMouseY = "Mouse Y";
    public const string tagLayerGround = "Ground";
    public const string tagLayerEnemy = "Enemy";
    public const string layerUntouchable = "UntouchablePhysics";
    public const string tagLayerPlayer = "Player";
    public const string tagLayerBlock = "Block";
    public const string tagAttack = "Attack";

    
    public const string masterMix = "Master";
    public const string musicMix = "Music";
    public const string sfxMix = "SFX";

}

public class GlobalScriptableString {
    public const string MapData = "MapData";
    public const string Scriptable = "Scriptable";
    public const string SoundBank = "SoundBank";
    public const string InventoryHolder = "InventoryHolderData";
}

public class GlobalPoolString {
    public const string Pool = "Pool";
    public const string VFXPrefab = "VFXPrefab";
}