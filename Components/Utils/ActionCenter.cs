using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCenter 
{
    public static Action<List<AnimationClip>> onAnimChange; 

    public static void InvokeWithList<T>(Action<List<T>> _anim, List<T> _list)
    {
        _anim?.Invoke(_list);
    }
}
