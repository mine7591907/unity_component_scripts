using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Sirenix.OdinInspector;

public class SystemSaver : SafeSingleton<SystemSaver>
{

    

    public void SavePlayerPrefJson<T>(T _obj, string _key)
    {
        string data = JsonConvert.SerializeObject(_obj);
        PlayerPrefs.SetString(_key, data);
        PlayerPrefs.Save();
    }

    public T LoadPlayerPrefJson<T>(string _key)
    {
        return JsonConvert.DeserializeObject<T>(PlayerPrefs.GetString(_key));
    }

    public void SaveEncryptedPlayerPrefJson<T>(T _obj, string _key)
    {
        string data = JsonConvert.SerializeObject(_obj);
        byte[] encrypt = EncryptionUtil.Instance.EncryptStringToBytes_Aes(data);
        string encryptedString = Convert.ToBase64String(encrypt);
        PlayerPrefs.SetString(_key, encryptedString);
        PlayerPrefs.Save();
    }

    public T LoadEncryptedPlayerPrefJson<T>(string _key)
    {
        string encryptedString = PlayerPrefs.GetString(_key);
        byte[] encryptContent = Convert.FromBase64String(encryptedString);
        string decrypt = EncryptionUtil.Instance.DecryptStringFromBytes_Aes(encryptContent);
        return JsonConvert.DeserializeObject<T>(decrypt);
    }

    public void SaveFile<T>(T _obj, string _path)
    {
        IOSystemic.SaveData(_obj, _path);
    } 

    public T LoadFile<T>(string _path)
    {
        return IOSystemic.LoadData<T>(_path);
    }

    [Button(ButtonHeight = 50)]
    public void ResetFile()
    {
        File.Delete(Application.persistentDataPath);
    }

    string GetPath(string _fileName)
    {
        return Application.persistentDataPath + _fileName;
    }


}
