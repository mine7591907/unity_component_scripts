using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockParry : MonoBehaviour
{
    [SerializeField] float idealTime = 0.3f;

    public bool isCanParry { get; private set; } = false;

    Coroutine stopParryTime;
    WaitForSeconds wait;

    private void Awake()
    {
        wait = new WaitForSeconds(idealTime);
    }

    public void PrepareParry()
    {
        isCanParry = true;
        stopParryTime = StartCoroutine(IEParryOff());
    }

    public void ParrySuccess()
    {
        isCanParry = false;
        StopCoroutine(stopParryTime);
    }

    IEnumerator IEParryOff()
    {
        yield return wait;
        isCanParry = false;
    }
}
