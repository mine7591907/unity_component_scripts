using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class PerfectDash : MonoBehaviour
{
    [SerializeField] float idealTime = 1f;
    [SerializeField] Collider triggerCollider;
    [SerializeField] GameObject characterGetHit;
    ICharacterGetHit iCharacterGetHit;

    WaitForSeconds wait;
    Coroutine disableCollider;
    Tween tween;

    private void Awake()
    {
        wait = new WaitForSeconds(idealTime);
        triggerCollider.enabled = false;
        iCharacterGetHit = characterGetHit.GetComponent<ICharacterGetHit>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GlobalString.tagAttack) == false) return;
        //Player.instance.ChangeLayerBecomeGhost();
        iCharacterGetHit.CantBeHit = true;
        Time.timeScale = 0;
        tween = null;
        tween = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1, 0.5f).SetEase(Ease.OutBack)
            .OnComplete(() =>
            {
                iCharacterGetHit.CantBeHit = false;
                //Player.instance.ChangeLayerNormal();
            });
        StopCoroutine(disableCollider);
        disableCollider = null;
        triggerCollider.enabled = false;
    }

    public void Active()
    {
        triggerCollider.enabled = true;
        disableCollider = StartCoroutine(IEDisableCollider());
    }

    IEnumerator IEDisableCollider()
    {
        yield return wait;
        triggerCollider.enabled = false;
    }




}
