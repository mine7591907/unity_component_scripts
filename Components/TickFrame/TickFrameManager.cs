using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


/// <summary>
/// Player setting \n
/// #if TICK_FRAME_MANAGER
/// </summary>
public class TickFrameManager : SafeSingleton<TickFrameManager>
{
    [SerializeField] List<ITickFrame> ticks = new List<ITickFrame>();

    public void AddTick(ITickFrame _tick)
    {
        ticks.Add(_tick);
    }

    public void RemoveTick(ITickFrame _tick)
    {
        ticks.Remove(_tick);
    }

    private void FixedUpdate()
    {
        foreach (var tick in ticks)
        {
            if (tick.GetGameObject().activeSelf == false) continue;
            tick.TickFixedUpdate();
        }
    }

    void Update()
    {
        foreach (var tick in ticks)
        {
            if (tick.GetGameObject().activeSelf == false) continue;
            tick.TickUpdate();
        }
    }



}
public interface ITickFrame
{
    public void TickUpdate();
    public void TickFixedUpdate();
    public GameObject GetGameObject();
    public void OnDestroy()
    {
        TickFrameManager.Instance.RemoveTick(this);
    }
}