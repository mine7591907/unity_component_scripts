using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = G"InputHandlerPlatform",
    menuName = "Scripable/" + "InputHandlerPlatform"
    )]
public class InputHandlerPlatformScriptable : SerializedScriptableObject
{
    [SerializeField] Dictionary<RuntimePlatform, PlatformData> platformDatas;
    public KeyCode GetInput(InputID _id, RuntimePlatform _platform)
    {
        return platformDatas[_platform].inputs[_id];
    }
    
    public class PlatformData
    {
        public Dictionary<InputID, KeyCode> inputs;
    }
}



public enum InputID
{
    MoveUp, MoveDown, MoveLeft, MoveRight, Crouch, Prone, Sprint, Walk,
    Slot1, Slot2, Slot3, Slot4,
    MouseLeftClick, MouseRightClick, MouseMiddleClick,
    Esc, ScoreBoard,
    EnterChat, ChangeChatMode,
    WatchMap,
}