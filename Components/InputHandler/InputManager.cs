using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : SafeSingleton<InputManager>
{
    InputHandlerPlatformScriptable data;
    RuntimePlatform platform;

    private void Awake()
    {
        CheckDataLoad();
        platform = Application.platform;
    }

    public bool GetInputPressing(InputID _id)
    {
        CheckDataLoad();
        return Input.GetKey(data.GetInput(_id, platform));
    }

    public bool GetInputPressed(InputID _id)
    {
        CheckDataLoad();
        return Input.GetKeyDown(data.GetInput(_id, platform));
    }

    public bool GetInputReleased(InputID _id)
    {
        CheckDataLoad();
        return Input.GetKeyUp(data.GetInput(_id, platform));
    }

    void CheckDataLoad()
    {
        if (data != null) return;
        data = Resources.Load<InputHandlerPlatformScriptable>("InputHandlerPlatform");
    }

    public float HorizontalAxis()
    {
        return Input.GetAxis(GlobalString.axisHorizontal);
    }

    public float VerticalAxis()
    {
        return Input.GetAxis(GlobalString.axisVertical);
    }


}

