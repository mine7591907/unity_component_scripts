using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

//public class ComplexDictionaryListPooling<INSTANCE, ENUM> : SafeSingleton<INSTANCE> where INSTANCE : Component
public class ComplexDictionaryListPooling<ENUM, OBJECT_TYPE> where OBJECT_TYPE : Component
{
    [ShowInInspector] Dictionary<ENUM, List<OBJECT_TYPE>> dictPool = new Dictionary<ENUM, List<OBJECT_TYPE>>();

    [ShowInInspector] Dictionary<ENUM, OBJECT_TYPE> prefab;

    public void Init(Dictionary<ENUM, OBJECT_TYPE> _prefab)
    {
        prefab = _prefab;
    }

    public OBJECT_TYPE GetObj(ENUM _enum, Transform _obj)
    {
        if (dictPool.ContainsKey(_enum))
        {
            for (int i = 0; i < dictPool[_enum].Count; i++)
            {
                if (dictPool[_enum][i].gameObject.activeSelf) continue;
                dictPool[_enum][i].gameObject.SetActive(true);
                return dictPool[_enum][i];
            }
        }
        else dictPool.Add(_enum, new List<OBJECT_TYPE>());
        var newObj = GameObject.Instantiate(prefab[_enum], _obj);
        dictPool[_enum].Add(newObj);
        return newObj;
    }

}
