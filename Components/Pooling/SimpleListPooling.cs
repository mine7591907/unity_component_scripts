using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SimpleListPooling : MonoBehaviour
{
    [ShowInInspector] List<GameObject> listPool = new List<GameObject>();
    [Space][ShowInInspector] GameObject prefab;

    public void Init(GameObject _prefab)
    {
        prefab = _prefab;
    }

    public GameObject GetObj()
    {
        for (int i = 0; i < listPool.Count; i++)
        {
            if (listPool[i].activeSelf) continue;
            return listPool[i];
        }
        var newObj = Instantiate(prefab, transform);
        listPool.Add(newObj);   
        return newObj;
    }



}
