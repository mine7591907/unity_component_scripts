using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class SafeSingleton<T> : SerializedMonoBehaviour where T : Component
{
    protected static T _instance;

    /// <summary>
    /// For Safe Singleton design pattern
    /// </summary>
    /// <value>The instance.</value>
    public static T Instance
    {
        get
        {
            Init(ref _instance);
            return _instance;
        }
    }

    public static void Init(ref T _instance)
    {
        if (_instance == null)
        {
            GameObject obj = new GameObject();
            _instance = obj.AddComponent<T>();
            obj.name = _instance.GetType().ToString() + "(generated)";
            DontDestroyOnLoad(obj);
        }
    }
    //public static void Init()
    //{
    //    if (_instance == null)
    //    {
    //        GameObject obj = new GameObject();
    //        _instance = obj.AddComponent<T>();
    //        obj.name = _instance.GetType().ToString() + "(generated)";
    //    }
    //}
}
