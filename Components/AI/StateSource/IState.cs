public interface IState<T>
{
    public void OnEnter();
    public void OnExit();
    public T OnUpdate();

    public void OnFixedUpdate();
}

public interface ICharacterState : IState<State>
{
    
}


