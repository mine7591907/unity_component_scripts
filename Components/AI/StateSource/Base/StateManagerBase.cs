using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class StateManagerBase<STATE_ENUM, IState_STATE_ENUM> : SerializedMonoBehaviour, ITickFrame where IState_STATE_ENUM : IState<STATE_ENUM>
{
    public Action<STATE_ENUM> onStateChange;

    [SerializeField] Dictionary<STATE_ENUM, GameObject> stateObjectDictionary;
    protected Dictionary<STATE_ENUM, IState_STATE_ENUM> statesDictionary = new Dictionary<STATE_ENUM, IState_STATE_ENUM>();
    [ShowInInspector] STATE_ENUM currentState;
    public STATE_ENUM GetCurrentState() { return currentState; }
    STATE_ENUM lastState;
    public STATE_ENUM LastState => lastState;

    void Awake()
    {
        foreach (var keyValue in stateObjectDictionary)
        {
            statesDictionary[keyValue.Key] = keyValue.Value.GetComponent<IState_STATE_ENUM>();
        }
        TickFrameManager.Instance.AddTick(this);
    }

    protected virtual void Start()
    {
        statesDictionary[currentState].OnEnter();
    }

    public void SetState(STATE_ENUM _state)
    {
        enabled = false;
        statesDictionary[currentState].OnExit();
        currentState = _state;
        statesDictionary[currentState].OnEnter();
        enabled = true;
    }

    public bool HasState(STATE_ENUM _state)
    {
        return statesDictionary.ContainsKey(_state);
    }



    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void TickFixedUpdate()
    {
        if (HasState(currentState) == false) return;
        statesDictionary[currentState].OnFixedUpdate();
    }

    public void TickUpdate()
    {
        Run();
    }
    void Run()
    {
        if (HasState(currentState) == false) return;
        STATE_ENUM nextState = statesDictionary[currentState].OnUpdate();

        // Check if the state needs to change
        if (EqualityComparer<STATE_ENUM>.Default.Equals(currentState, nextState) == false)
        {
            // Exit current state
            statesDictionary[currentState].OnExit();

            lastState = currentState;
            // Switch to the new state
            currentState = nextState;

            // Enter the new state
            statesDictionary[nextState].OnEnter();
            onStateChange?.Invoke(currentState);
        }
    }
}
