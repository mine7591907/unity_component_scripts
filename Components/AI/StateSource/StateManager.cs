using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
public class StateManager<State, T2> : SerializedMonoBehaviour, ITickFrame
    where T2 : IState<State>
{
    public Action<State> onStateChange;

    [SerializeField] Dictionary<State, GameObject> stateObjectDictionary;
    protected Dictionary<State, T2> statesDictionary = new Dictionary<State, T2>();

    [SerializeField] private State currentState;
    public State GetCurrentState() { return currentState; }

    State lastState;
    public State LastState => lastState;

    protected virtual void Awake()
    {
        foreach (var keyValue in stateObjectDictionary)
        {
            statesDictionary[keyValue.Key] = keyValue.Value.GetComponent<T2>();
        }
        TickFrameManager.Instance.AddTick(this);
    }

    protected virtual void Start()
    {
        statesDictionary[currentState].OnEnter();
    }
    /// <summary>
    /// Don't call it at Awake
    /// </summary>
    /// <param name="_newState"></param>
    public void SetState(State _newState)
    {
        enabled = false;
        statesDictionary[currentState].OnExit();
        currentState = _newState;
        statesDictionary[currentState].OnEnter();
        enabled = true;
    }

    public bool HasState(State _state)
    {
        return statesDictionary.ContainsKey(_state);
    }
    public GameObject GetGameObject()
    {
        return gameObject;
    }
    public void TickUpdate()
    {
        Run();
    }

    public void TickFixedUpdate()
    {
        if (HasState(currentState) == false) return;
        statesDictionary[currentState].OnFixedUpdate();
    }
    void Update()
    {
        Run();
    }

    private void FixedUpdate()
    {
        if (HasState(currentState) == false) return;
        statesDictionary[currentState].OnFixedUpdate();
    }

    void Run()
    {
        if (HasState(currentState) == false) return;
        State nextState = statesDictionary[currentState].OnUpdate();

        // Check if the state needs to change
        if (EqualityComparer<State>.Default.Equals(currentState, nextState) == false)
        {
            // Exit current state
            statesDictionary[currentState].OnExit();

            lastState = currentState;
            // Switch to the new state
            currentState = nextState;

            // Enter the new state
            statesDictionary[nextState].OnEnter();
            onStateChange?.Invoke(currentState);
        }
    }

}

public interface IStateManagerSetState {
    public void SetState(State _newState);
}


public enum State
{
    Idle = 0, Walking = 1, Running = 2, Jogging = 3, JogToStop = 4, 
    Jump = 7, Fall = 9, Land = 6,
    Roll = 5, Dash = 8,
    Block = 10, SheathWeapon = 11, Parry = 15, BeingParried = 16,
    BlockHitImpact = 12, GetHit = 13, Die = 14, 
    NormalAttack = 20 ,NormalAttack1 = 21, NormalAttack2 = 22, NormalAttack3 = 23,
    




    Thinking = -99

}
