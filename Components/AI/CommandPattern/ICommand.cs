
public interface ICommand 
{
    public bool IsComplete {get; set; }

    public void Execute();

    public void OnDone() { IsComplete = false; }

}
