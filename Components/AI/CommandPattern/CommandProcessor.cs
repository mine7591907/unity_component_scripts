using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CommandProcessor : MonoBehaviour
{
    [SerializeField] List<GameObject> editorCommands;
    List<ICommand> commands = new List<ICommand>();

    Coroutine execution;

    private void Awake()
    {
        if (editorCommands == null || editorCommands.Count == 0) return;
        for (int i = 0; i < editorCommands.Count; i++)
        {
            commands.Add(editorCommands[i].GetComponent<ICommand>());
        }
    }

    public void AddCommand(ICommand _command)
    {
        commands.Add(_command);
    }

    public void ClearCommand() { commands.Clear(); }

    public void ExecuteImmediately()
    {
        for (int i = 0; i < commands.Count; i++)
        {
            commands[i].Execute();
        }
    }

    public void ExecuteSequencely()
    {
        execution = StartCoroutine(IEExecuteSequencely(null));
    }

    public void ExecuteSequencely(Action _callback)
    {
        execution = StartCoroutine(IEExecuteSequencely(_callback));
    }

    public void StopSequence()
    {
        StopCoroutine(execution);
    }

    IEnumerator IEExecuteSequencely(Action _callBack)
    {
        for (int i = 0; i < commands.Count;i++)
        {
            commands[i].IsComplete = false;
            commands[i].Execute();
            yield return new WaitUntil(() => commands[i].IsComplete);
            commands[i].OnDone();
        }
        _callBack?.Invoke();
    }


}
