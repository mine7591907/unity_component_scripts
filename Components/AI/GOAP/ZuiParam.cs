using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ZuiParam 
{
    [HideInInspector] public object value;
    public abstract void InIt();
}

public class ZuiInt : ZuiParam
{
    [SerializeField] int myInt;
    public ZuiInt(int _value)
    {
        myInt = _value;
        value = myInt;
    }

    public override void InIt()
    {
        value = myInt;
    }
}

public class ZuiFloat : ZuiParam
{
    [SerializeField] float myFloat;
    public ZuiFloat(float _value)
    {
        myFloat = _value;
        value = myFloat;
    }

    public override void InIt()
    {
        value = myFloat;
    }
}

public class ZuiBool : ZuiParam
{
    [SerializeField] bool myBool;
    public ZuiBool(bool _value) {  
        myBool = _value;
        value = myBool;
    }

    public override void InIt()
    {
        value = myBool;
    }
}

public class ZuiString : ZuiParam
{
    [SerializeField] string myString;
    public ZuiString(string _value) { 
        myString = _value;
        value = myString;
    }

    public override void InIt()
    {
        value = myString;
    }
}

