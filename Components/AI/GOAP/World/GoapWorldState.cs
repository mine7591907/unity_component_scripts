﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GoapWorldState : GoapStateBase<GoapWorldStateEnum>
{
    public static GoapWorldState _instance;

    public static GoapWorldState Instance
    {
        get
        {
            SafeSingleton<GoapWorldState>.Init(ref  _instance);
            return _instance;
        }
    }
}

public enum GoapWorldStateEnum
{
    int_PlayerHealth, bool_PlayerLow20Per, bool_PlayerHalfHealth, int_PlayerCurrentNormalMeleeAttackIndex,
    int_PlayerCurrentWeaponIndex,


}

