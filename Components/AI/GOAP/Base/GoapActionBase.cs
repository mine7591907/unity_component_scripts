using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoapActionBase<T> : SerializedMonoBehaviour
{
    public CommandProcessor commandAction;
    public Dictionary<T, ZuiParam> preconditions;
    [Space]
    public Dictionary<T, ZuiParam> effects;
    public float cost;

    private void Awake()
    {
        foreach (var value in preconditions.Values)
        {
            value.InIt();
        }
        foreach (var value in effects.Values)
        {
            value.InIt();
        }
    }

    /// <summary>
    /// Check if the action's preconditions are satisfied in the current state
    /// </summary>
    /// <param name="worldState"></param>
    /// <returns></returns>
    public bool CheckPreconditions(Dictionary<T, ZuiParam> worldState)
    {
        foreach (var precondition in preconditions)
        {
            bool check = !worldState.ContainsKey(precondition.Key);
            bool check2 = worldState.ContainsKey(precondition.Key) && !worldState[precondition.Key].value.Equals(precondition.Value.value);
            //if (!worldState.ContainsKey(precondition.Key) || !worldState[precondition.Key].Equals(precondition.Value))
            if (check || check2)
            {
                return false;
            }
        }
        return true;
    }

    public void ApplyWorldEffects(GoapStateBase<T> _state)
    {
        foreach (var effect in effects)
        {
            _state.SetState(effect.Key, effect.Value);
        }
    }

    public Dictionary<T, ZuiParam> ApplyEffects(Dictionary<T, ZuiParam> worldState)
    {
        var newWorldState = new Dictionary<T, ZuiParam>(worldState);
        foreach (var effect in effects)
        {
            newWorldState[effect.Key] = effect.Value;
        }
        return newWorldState;
    }







}
