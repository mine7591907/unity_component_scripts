using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GoapPlannerBase<T> 
{
     List<GoapActionBase<T>> availableActions;

    public void SetActions(List<GoapActionBase<T>> _actions)
    {
        availableActions = _actions;
    }

    List<Node> openSet = new List<Node>();
    List<Node> closedSet = new List<Node>();
    //Dictionary<T, ZuiParam> newState;


    public List<GoapActionBase<T>> Plan(Dictionary<T, ZuiParam> currentState, GoapGoalBase<T> goal)
    {
        // Initialize open and closed sets for the A* algorithm
        openSet = new List<Node>();
        closedSet = new List<Node>();

        // Create the starting node with the initial world state
        Node startNode = new Node(null, 0, currentState, null);

        // Add the starting node to the open set
        openSet.Add(startNode);

        // A* search algorithm loop
        while (openSet.Count > 0)
        {
            // Select the node with the lowest combined cost and heuristic
            Node currentNode = openSet.OrderBy(n => n.Cost + Heuristic(n.State, goal.GetConditions())).First();

            // Check if the goal state is reached
            if (goal.CheckGoalState(currentNode.State))
            {
                // Goal reached, construct and return the plan
                return ReconstructPlan(currentNode);
            }

            // Move the current node from open set to closed set
            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            // Explore available actions
            foreach (var action in availableActions)
            {
                // Check if the action's preconditions are satisfied in the current state
                if (action.CheckPreconditions(currentNode.State))
                {
                    // Apply the action's effects to get the new state
                    Dictionary<T, ZuiParam> effects = action.ApplyEffects(currentNode.State);
                    //newState = action.ApplyEffects(currentNode.State);

                    // Create a new node representing the result of applying the action
                    Node newNode = new Node(currentNode, currentNode.Cost + action.cost, effects, action);

                    // Check if the new state has not been explored or has a lower cost
                    if (!closedSet.Any(n => n.State.SequenceEqual(effects)))
                    {
                        Node existingNode = openSet.FirstOrDefault(n => n.State.SequenceEqual(effects));

                        // If the new state is not in the open set or has a lower cost, add it to the open set
                        if (existingNode == null || newNode.Cost < existingNode.Cost)
                        {
                            openSet.Add(newNode);
                        }
                    }
                }
            }
        }
        // No valid plan found
        return null;
    }

    private List<GoapActionBase<T>> ReconstructPlan(Node goalNode)
    {
        // Reconstruct the plan by backtracking from the goal node
        List<GoapActionBase<T>> plan = new List<GoapActionBase<T>>();

        Node currentNode = goalNode;

        // Traverse the path from the goal node to the start node
        while (currentNode.Parent != null)
        {
            plan.Insert(0, currentNode.Action); // Insert at the beginning to maintain order
            currentNode = currentNode.Parent;
        }

        return plan;
    }

    private float Heuristic(Dictionary<T, ZuiParam> state, Dictionary<T, ZuiParam> goal)
    {
        // Basic heuristic: Count the number of differences between the current state and the goal
        return state.Count(kv => !goal.ContainsKey(kv.Key) || !goal[kv.Key].Equals(kv.Value));
    }

    private class Node
    {
        public Node Parent;
        public float Cost;
        public Dictionary<T, ZuiParam> State;
        public GoapActionBase<T> Action;

        public Node(Node parent, float cost, Dictionary<T, ZuiParam> state, GoapActionBase<T> action)
        {
            Parent = parent;
            Cost = cost;
            State = state;
            Action = action;
        }
    }
}
