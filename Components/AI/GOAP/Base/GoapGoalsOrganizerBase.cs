using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GoapGoalsOrganizerBase<T> : MonoBehaviour
{
    [SerializeField] List<GoapGoalBase<T>> goals;

    public GoapGoalBase<T> SelectGoal()
    {
        if (goals == null || goals.Count == 0) return null;
        return goals.OrderBy(goal => goal.priority).First();
    }

}
