using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;
public class GoapAgent<LOCAL_STATE>
{
    public PlanOrientation planOrientation;
    public float timingPlan = 1;

    GoapGoalsOrganizerBase<LOCAL_STATE> localGoalOrganize;
    GoapStateBase<LOCAL_STATE> localState;
    GoapPlannerBase<LOCAL_STATE> localPlanner;
    GoapWorldStateGoalsOrganizer worldGoalOrganize;
    GoapWorldStatePlanner worldPlanner;

    public GoapAgent(GoapGoalsOrganizerBase<LOCAL_STATE> localOrganize
        , GoapStateBase<LOCAL_STATE> localState
        , GoapWorldStateGoalsOrganizer worldOrganize
        , GoapWorldStatePlanner worldPlanner
        , GoapPlannerBase<LOCAL_STATE> localPlanner)
    {
        this.localGoalOrganize = localOrganize;
        this.localState = localState;
        this.worldGoalOrganize = worldOrganize;
        this.worldPlanner = worldPlanner;
        this.localPlanner = localPlanner;
    }
    List<GoapActionBase<LOCAL_STATE>> localActions = new List<GoapActionBase<LOCAL_STATE>>();
    Coroutine iePlan;

    public void Start()
    {
        iePlan  = GoapWorldState.Instance.StartCoroutine(IEPlan());
    }

    public void Stop()
    {
        GoapWorldState.Instance.StopCoroutine(iePlan);
    }

    IEnumerator IEPlan()
    {
        yield return new WaitForSeconds(timingPlan);
        ChosePlan();
    }

    void ChosePlan()
    {
        switch (planOrientation)
        {
            case PlanOrientation.LocalPrior:
                localActions = localPlanner.Plan(localState.GetStates(), localGoalOrganize.SelectGoal());
                if (localActions == null || localActions.Count == 0)
                    PlanWorld();
                else
                    GoapWorldState.Instance.StartCoroutine(IEDoAction(localActions, localState));
                break;
            case PlanOrientation.WorldPrior:
                worldActions = worldPlanner.Plan(GoapWorldState.Instance.GetStates(), worldGoalOrganize.SelectGoal());
                if (worldActions == null || worldActions.Count == 0)
                    PlanLocal();
                else
                    GoapWorldState.Instance.StartCoroutine(IEDoAction<GoapWorldStateEnum>(worldActions, GoapWorldState.Instance));
                break;
            case PlanOrientation.CompareTotalCost:
                float localCost = 0;
                float worldCost = 0;
                localActions = localPlanner.Plan(localState.GetStates(), localGoalOrganize.SelectGoal());
                worldActions = worldPlanner.Plan(GoapWorldState.Instance.GetStates(), worldGoalOrganize.SelectGoal());
                for (int i = 0; i < localActions.Count; i++){
                    localCost += localActions[i].cost;
                }
                for (int i = 0; i < worldActions.Count; i++) {
                    worldCost += worldActions[i].cost;
                }
                if (localCost < worldCost) PlanWorld();
                else PlanLocal();
                break;
        }
    }

    void PlanLocal()
    {
        localActions = localPlanner.Plan(localState.GetStates(), localGoalOrganize.SelectGoal());
        if (localActions == null || localActions.Count == 0)
        {
            iePlan = GoapWorldState.Instance.StartCoroutine(IEPlan());
        }
        else
        {
            GoapWorldState.Instance.StartCoroutine(IEDoAction(localActions, localState));
        }
    }
    List<GoapActionBase<GoapWorldStateEnum>> worldActions = new List<GoapActionBase<GoapWorldStateEnum>>();
    void PlanWorld()
    {
        if (GoapWorldState.Instance.GetStates() == null || GoapWorldState.Instance.GetStates().Count == 0 || worldGoalOrganize.SelectGoal() == null)
        {
            iePlan = GoapWorldState.Instance.StartCoroutine(IEPlan());
            return;
        }
        worldActions = worldPlanner.Plan(GoapWorldState.Instance.GetStates(), worldGoalOrganize.SelectGoal());
        if (worldActions == null || worldActions.Count == 0)
        {
            iePlan = GoapWorldState.Instance.StartCoroutine(IEPlan());
        }
        else
        {
            GoapWorldState.Instance.StartCoroutine(IEDoAction<GoapWorldStateEnum>(worldActions, GoapWorldState.Instance));
        }

    }

    IEnumerator IEDoAction<T>(List<GoapActionBase<T>> _actions, GoapStateBase<T> _state)
    {
        for (int i = 0; i < _actions.Count; i++)
        {
            bool ok = false;
            _actions[i].commandAction.ExecuteSequencely(() =>
                ok = true
            ); ;
            yield return new WaitUntil(() => ok);
            //yield return GoapWorldState.Instance.StartCoroutine(_actions[i].commandAction.IEExecuteSequencely());
            _actions[i].ApplyWorldEffects(_state);
        }
        Start();
    }

    public enum PlanOrientation
    {
        LocalPrior, WorldPrior, CompareTotalCost
    }

}
