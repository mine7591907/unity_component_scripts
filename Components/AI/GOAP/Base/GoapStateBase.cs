using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoapStateBase<T> : MonoBehaviour
{
    [ShowInInspector]
    Dictionary<T, ZuiParam> states = new Dictionary<T, ZuiParam>();



    public void SetState(T _type, ZuiParam _value)
    {
        states[_type] = _value;
    }

    public bool HasState(T _type) => states.ContainsKey(_type);

    public Dictionary<T, ZuiParam> GetStates() => states;


}
