using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoapGoalBase<T> : SerializedMonoBehaviour
{
    //public string GoalName;
    [SerializeField] Dictionary<T, ZuiParam> GoalConditions;
    public int priority = 0;

    private void Awake()
    {
        foreach (var value in GoalConditions.Values)
        {
            value.InIt();
        }
    }

    public Dictionary<T, ZuiParam> GetConditions() => GoalConditions;

    /// <summary>
    /// Check if the goal state is reached
    /// </summary>
    /// <param name="worldState"></param>
    /// <returns></returns>
    public bool CheckGoalState(Dictionary<T, ZuiParam> worldState)
    {
        foreach (var condition in GoalConditions)
        {
            bool check = !worldState.ContainsKey(condition.Key);
            bool check2 = worldState.ContainsKey(condition.Key) && !worldState[condition.Key].value.Equals(condition.Value.value);
            //if (!worldState.ContainsKey(condition.Key) ||(worldState.ContainsKey(condition.Key) && !worldState[condition.Key].Equals(condition.Value)))
            if (check || check2)
            {
                return false;
            }
        }
        return true;
    }
}
