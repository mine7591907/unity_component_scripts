using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//public class __GoapPlanner
//{
//    public List<GoapAction> Plan(List<GoapAction> availableActions, Dictionary<GoapStateEnum, object> currentState, GoapGoal goal)
//    {
//        List<Node> openSet = new List<Node>();
//        List<Node> closedSet = new List<Node>();

//        Node startNode = new Node(null, 0, currentState, null);

//        openSet.Add(startNode);

//        while (openSet.Count > 0)
//        {
//            Node currentNode = openSet.OrderBy(n => n.Cost + Heuristic(n.State, goal.GoalConditions)).First();

//            if (goal.CheckGoalState(currentNode.State))
//            {
//                // Goal reached, construct and return the plan
//                return ReconstructPlan(currentNode);
//            }

//            openSet.Remove(currentNode);
//            closedSet.Add(currentNode);

//            foreach (var action in availableActions)
//            {
//                if (action.CheckPreconditions(currentNode.State))
//                {
//                    Dictionary<GoapStateEnum, object> newState = action.ApplyEffects(currentNode.State);

//                    Node newNode = new Node(currentNode, currentNode.Cost + action.cost, newState, action);

//                    if (!closedSet.Any(n => n.State.SequenceEqual(newState)))
//                    {
//                        Node existingNode = openSet.FirstOrDefault(n => n.State.SequenceEqual(newState));

//                        if (existingNode == null || newNode.Cost < existingNode.Cost)
//                        {
//                            openSet.Add(newNode);
//                        }
//                    }
//                }
//            }
//        }

//        // No valid plan found
//        return null;
//    }

//    private List<GoapAction> ReconstructPlan(Node goalNode)
//    {
//        List<GoapAction> plan = new List<GoapAction>();

//        Node currentNode = goalNode;

//        while (currentNode.Parent != null)
//        {
//            plan.Insert(0, currentNode.Action); // Insert at the beginning to maintain order
//            currentNode = currentNode.Parent;
//        }

//        return plan;
//    }

//    private float Heuristic(Dictionary<GoapStateEnum, object> state, Dictionary<GoapStateEnum, object> goal)
//    {
//        // Basic heuristic: Count the number of differences between the current state and the goal
//        return state.Count(kv => !goal.ContainsKey(kv.Key) || !goal[kv.Key].Equals(kv.Value));
//    }

//    private class Node
//    {
//        public Node Parent;
//        public float Cost;
//        public Dictionary<GoapStateEnum, object> State;
//        public GoapAction Action;

//        public Node(Node parent, float cost, Dictionary<GoapStateEnum, object> state, GoapAction action)
//        {
//            Parent = parent;
//            Cost = cost;
//            State = state;
//            Action = action;
//        }
//    }
//}

//public class GOAPExample : MonoBehaviour
//{
//    private Dictionary<string, object> worldState;

//    private void Start()
//    {
//        // Initialize world state
//        worldState = new Dictionary<string, object>
//        {
//            { "hasAxe", false },
//            { "hasWood", false },
//            { "isHungry", true }
//        };

//        // Example GOAPAction
//        GOAPAction chopWoodAction = new GOAPAction("ChopWood", 2.0f);
//        chopWoodAction.Preconditions.Add("hasAxe", true);
//        chopWoodAction.Effects.Add("hasWood", true);

//        // Example GOAPGoal
//        GOAPGoal gatherResourcesGoal = new GOAPGoal("GatherResources");
//        gatherResourcesGoal.GoalConditions.Add("hasWood", true);

//        // Example GOAPPlanner
//        GOAPPlanner planner = new GOAPPlanner();

//        // Plan and execute
//        List<GOAPAction> plan = planner.Plan(new List<GOAPAction> { chopWoodAction }, worldState, gatherResourcesGoal);

//        if (plan != null && plan.Count > 0)
//        {
//            Debug.Log("Plan:");
//            foreach (var action in plan)
//            {
//                Debug.Log(action.ActionName);
//            }
//        }
//        else
//        {
//            Debug.Log("No valid plan found.");
//        }
//    }
//}