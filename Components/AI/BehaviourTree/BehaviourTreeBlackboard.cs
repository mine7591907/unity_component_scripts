using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTreeBlackboard<T, ENUM_INFO> 
{
    [ShowInInspector] T agent;
    [ShowInInspector] Dictionary<ENUM_INFO, ZuiParam> agentInfo;


    BehaviourTreeResult lastResult;
    public BehaviourTreeResult LastResult => lastResult;


    public BehaviourTreeBlackboard(T _agent)
    {
        agent = _agent;
    }

    public T GetAgent() { 

        return agent;
    }

    public void SetLastResult(BehaviourTreeResult _lastResult)
    {
        lastResult = _lastResult;
    }

    //public void AssignBlackboard(ENUM_INFO _info, ZuiParam _param)
    //{
    //    agentInfo[_info] = _param;
    //}

    ZuiBool boolean = new ZuiBool(false);
    ZuiFloat floater = new ZuiFloat(0);
    ZuiInt interger = new ZuiInt(0);
    public void AssignBlackBoard(ENUM_INFO _info, bool _result)
    {
        boolean.value = _result;
        agentInfo[_info] = boolean;
    }

    public void AssignBlackBoard(ENUM_INFO _info, int _result)
    {
        interger.value = _result;
        agentInfo[_info] = interger;
    }

    public void AssignBlackBoard(ENUM_INFO _info, float _result)
    {
        floater.value = _result;
        agentInfo[_info] = floater;
    }

    public int GetInfoInt(ENUM_INFO _info)
    {
        if (agentInfo.ContainsKey(_info) == false) return -1;
        return (int)agentInfo[_info].value;
    }

    public float GetInfoFloat(ENUM_INFO _info)
    {
        if (agentInfo.ContainsKey(_info) == false) return -1;
        return (float)agentInfo[_info].value;
    }

    public bool GetInfo(ENUM_INFO _info)
    {
        if (agentInfo.ContainsKey(_info) == false)
            return false;
        return (bool)agentInfo[_info].value ;
    }



}

public enum BehaviourTreeBlackboardInfo
{
    PlayerLowHeath,
    PlayerFullHealth,
    SelfEnemyLowHealth,

    EnemyAbilityOneReady,
    EnemyAbilityTwoReady,
    EnemyAbilityThreeReady,
    EnemyAbilityFourReady,
    EnemyAbilityFiveReady,
    EnemyAbilitySixReady,

    NormalAttackReady,
    
    NearPlayer,
    PlayerAtFar,
}
