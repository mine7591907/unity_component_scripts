using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTreeSequence<T, ENUM_INFO> : MonoBehaviour, IBehaviourTree<T, ENUM_INFO>
{
    [SerializeField] bool disabled = false;
    IBehaviourTree<T, ENUM_INFO> lastChild;
    List<IBehaviourTree<T, ENUM_INFO>> childs = new List<IBehaviourTree<T, ENUM_INFO>>();

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            childs.Add(transform.GetChild(i).GetComponent<IBehaviourTree<T, ENUM_INFO>>());
        }
    }

    public bool IsDisabled()
    {
        return disabled;
    }

    public BehaviourTreeResult Tick(BehaviourTreeBlackboard<T, ENUM_INFO> _blackboard)
    {
        if (_blackboard.LastResult == BehaviourTreeResult.Running)
        {
            return lastChild.Tick(_blackboard);
        }
        for (int i = 0; i < childs.Count; i++)
        {
            BehaviourTreeResult result = childs[i].Tick(_blackboard);
            lastChild = childs[i];
            switch (result)
            {
                case BehaviourTreeResult.Sucess:
                    continue;
                case BehaviourTreeResult.Running:
                    return result;
                case BehaviourTreeResult.Fail:
                    break;
            }
            break;
        }
        return BehaviourTreeResult.Sucess;
    }
}
