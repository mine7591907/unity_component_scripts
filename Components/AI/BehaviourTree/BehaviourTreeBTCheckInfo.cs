using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTreeBTCheckInfo<T, ENUM_INFO> : MonoBehaviour, IBehaviourTree<T, ENUM_INFO>
{
    [SerializeField] bool disabled;
    [SerializeField] ENUM_INFO info;
    BehaviourTreeBlackboard<T, ENUM_INFO> blackboard;

    public bool IsDisabled()
    {
        return disabled;
    }


    public BehaviourTreeResult Tick(BehaviourTreeBlackboard<T, ENUM_INFO> _blackboard)
    {
        return blackboard.GetInfo(info)? BehaviourTreeResult.Sucess : BehaviourTreeResult.Fail;
    }

}
