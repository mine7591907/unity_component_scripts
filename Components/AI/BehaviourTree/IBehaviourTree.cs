using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBehaviourTree<T, ENUM_INFO>
{
    //public void Setup();
    //public void SetBlackboard(BehaviourTreeBlackboard _blackboard);

    public BehaviourTreeResult Tick(BehaviourTreeBlackboard<T, ENUM_INFO> _blackboard);
    public bool IsDisabled();

}

public enum BehaviourTreeResult
{
    Sucess, Running, Fail
}
