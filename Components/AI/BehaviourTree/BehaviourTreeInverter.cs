using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTreeInverter<T, ENUM_INFO> : MonoBehaviour, IBehaviourTree<T, ENUM_INFO>
{
    [SerializeField] bool disabled = false;
    IBehaviourTree<T, ENUM_INFO> onlyChild = null;

    void Start()
    {
        if (transform.GetChild(0) == null ) return;
        onlyChild = transform.GetChild(0).GetComponent<IBehaviourTree<T, ENUM_INFO>>();
    }

    public bool IsDisabled()
    {
        return disabled;
    }

    public BehaviourTreeResult Tick(BehaviourTreeBlackboard<T, ENUM_INFO> _blackboard)
    {
        BehaviourTreeResult result = onlyChild.Tick(_blackboard);
        if (result == BehaviourTreeResult.Running) return result;
        return result == BehaviourTreeResult.Sucess? BehaviourTreeResult.Fail : BehaviourTreeResult.Sucess;
    }
}
