using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class BehaviourTreeSimpleAction<T, ENUM_INFO> : SerializedMonoBehaviour, IBehaviourTree<T, ENUM_INFO>
{
    [SerializeField] bool disabled = false;
    [SerializeField] UnityEvent methodEvent;

    public bool IsDisabled()
    {
        return disabled;
    }

    public BehaviourTreeResult Tick(BehaviourTreeBlackboard<T, ENUM_INFO> _blackboard)
    {
        methodEvent.Invoke();
        return BehaviourTreeResult.Sucess;
    }
}
