using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class BehaviourTree<T, ENUM_INFO> : SerializedMonoBehaviour
{
    [SerializeField] IBehaviourTree<T, ENUM_INFO> baseBranch;
    [SerializeField] T agent;
    [Header("Debug")]
    [ShowInInspector] BehaviourTreeBlackboard<T, ENUM_INFO> blackboard ;

    private void Start()
    {
        blackboard = new BehaviourTreeBlackboard<T, ENUM_INFO>(agent);
    }

    public void OnUpdate()
    {
        blackboard.SetLastResult(baseBranch.Tick(blackboard));
    }

    public BehaviourTreeBlackboard<T, ENUM_INFO> GetBlackboard()
    {
        return blackboard;
    }

}

//public interface IBTBlackboard
//{

//    public BehaviourTreeResult lastResult { get => lastResult; set => lastResult = value; }

//    public void SetInterfaceLastResult(BehaviourTreeResult _lastResult)
//    {
//        lastResult = _lastResult;
//    }
//    public Dictionary<BehaviourTreeBlackboardInfo, bool> agentInfo { get; set; }
//    public void AssignBlackBoard(BehaviourTreeBlackboardInfo _info, bool _result)
//    {
//        agentInfo[_info] = _result;
//    }
//    public bool GetInfo(BehaviourTreeBlackboardInfo _info)
//    {
//        if (agentInfo.ContainsKey(_info) == false)
//            return false;
//        return agentInfo[_info];
//    }

//    public IBTBlackboard GetBlackboard();

//}