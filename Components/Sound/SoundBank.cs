﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = GlobalScriptableString.SoundBank, menuName = GlobalScriptableString.Scriptable + "/" + GlobalScriptableString.SoundBank)]
public class SoundBank : SerializedScriptableObject
{
    [SerializeField] Dictionary<SoundIDSource, SoundData> bankData = new Dictionary<SoundIDSource, SoundData>();

    public AudioClip GetAudioClip(SoundIDSource _source, int _id)
    {
        return bankData[_source].GetAudio(_id);
    }

}


public enum SoundIDSource { 
    Environment ,Player, ExampleBoss,
    ExampleMeleeWeapon,

}

public enum SoundIDSource_Environment
{
    CharacterSlashed = 0,

}
