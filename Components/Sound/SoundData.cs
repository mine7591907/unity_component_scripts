﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "SoundIDSource_", menuName = "SoundBank")]
[Serializable]
public class SoundData /*: SerializedScriptableObject*/
{
    [SerializeField] Dictionary<int, AudioClip> data;

    public AudioClip GetAudio(int _type)
    {
        if (data.ContainsKey(_type) == false)
        {

        }
        return data[_type];
    }
}
