﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class SoundManager : SafeSingleton<SoundManager>
{
    const float minDb = -40;
    const float maxDb = 0;

    [Header("Debug")]
    [ShowInInspector] AudioMixer mixer;
    [ShowInInspector] List<SoundItem> items = new List<SoundItem>();
    [ShowInInspector] SoundItem bgm;
    [ShowInInspector] SoundBank bank;

    public float SfxVolumeNormalized
    {
        get => PlayerPrefs.GetFloat(PlayerPrefKeys.sfxVolumeNormalized, 1);
        set => PlayerPrefs.SetFloat(PlayerPrefKeys.sfxVolumeNormalized, value);
    }
    public float MusicVolumeNormalized
    {
        get => PlayerPrefs.GetFloat(PlayerPrefKeys.musicVolumeNormalized, 1);
        set => PlayerPrefs.SetFloat(PlayerPrefKeys.musicVolumeNormalized, value);
    }
    public float MasterVolumeNormalized
    {
        get => PlayerPrefs.GetFloat(PlayerPrefKeys.masterVolumeNormalized, 1);
        set => PlayerPrefs.SetFloat(PlayerPrefKeys.masterVolumeNormalized, value);
    }

    private void Awake()
    {
        //mixer = Resources.Load<AudioMixer>("MainAudioMixer");
        StartCoroutine(IELoad());
        SetVolume(AudioMixerGroupType.Master, MasterVolumeNormalized);
        SetVolume(AudioMixerGroupType.Music, MusicVolumeNormalized);
        SetVolume(AudioMixerGroupType.Sfx, SfxVolumeNormalized);
        bgm = GenSoundItem();
        bgm.SetMixerGroup(GetMixerGroup(AudioMixerGroupType.Music));
    }
    public bool initOk { private set; get; } = false;
    IEnumerator IELoad()
    {
        ResourceRequest mixerRequest = Resources.LoadAsync("MainAudioMixer");
        ResourceRequest bankRequest = Resources.LoadAsync(GlobalScriptableString.SoundBank);
        yield return new WaitUntil(()=> mixerRequest.isDone && bankRequest.isDone);
        mixer = mixerRequest.asset as AudioMixer;
        bank = bankRequest.asset as SoundBank;
        initOk = true;
    }

    public void PlayFx(SoundIDSource _source, int _index, AudioMixerGroupType _mixerType, bool _loop = false)
    {
        PlayFx(bank.GetAudioClip(_source, _index), _mixerType, _loop);
    }

    public void PlayFx(AudioClip _clip, AudioMixerGroupType _mixerType, bool _loop = false)
    {
        foreach (var item in items)
        {
            if (item.IsPlaying()) continue;
            item.PlaySound(_clip, _loop);
            break;
        }
        GenSoundItem().PlaySound(_clip, _loop).SetMixerGroup(GetMixerGroup(_mixerType));
    }

    public void PlayBGM(AudioClip _clip, bool _loop = false)
    {
        bgm.PlaySound(_clip, _loop);
    }

    public void SetVolume(AudioMixerGroupType _type, float _volumeNormal)
    {
        switch (_type)
        {
            case AudioMixerGroupType.Master:
                mixer.SetFloat(GlobalString.masterMix, GetDb(_volumeNormal));
                MasterVolumeNormalized = _volumeNormal;
                break;
            case AudioMixerGroupType.Music:
                mixer.SetFloat(GlobalString.musicMix, GetDb(_volumeNormal));
                MusicVolumeNormalized = _volumeNormal;
                break;
            case AudioMixerGroupType.Sfx:
                mixer.SetFloat(GlobalString.sfxMix, GetDb(_volumeNormal));
                SfxVolumeNormalized = _volumeNormal;
                break;
        }
    }

    float GetDb(float _normal)
    {
        float total = maxDb + minDb;
        float value = _normal == 0 ? -Mathf.Abs(_normal * total) + minDb : -80;
        return value;
    }
    SoundItem GenSoundItem()
    {
        SoundItem item = (new GameObject()).AddComponent<SoundItem>();
        //item.source = item.gameObject.AddComponent<AudioSource>();
        item.Init(item.gameObject.AddComponent<AudioSource>());
        return item;
    }

    AudioMixerGroup GetMixerGroup(AudioMixerGroupType _type)
    {
        switch (_type)
        {
            case AudioMixerGroupType.Master:
                return mixer.FindMatchingGroups(GlobalString.masterMix)[0];
            case AudioMixerGroupType.Music:
                return mixer.FindMatchingGroups(GlobalString.musicMix)[0];
            case AudioMixerGroupType.Sfx:
                return mixer.FindMatchingGroups(GlobalString.sfxMix)[0];
            default:
                return mixer.FindMatchingGroups(GlobalString.masterMix)[0];
        }
    }

}

public enum AudioMixerGroupType
{
    Master, Music, Sfx
}