﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;


public class SoundItem : MonoBehaviour
{
    [ShowInInspector] AudioSource source;

    public bool IsPlaying()=> source.isPlaying;

    public void Init(AudioSource _source)
    {
        source = _source;
    }

    public SoundItem PlaySound(AudioClip _clip, bool _loop = false)
    {
        source.clip = _clip;
        source.loop = _loop;
        source.volume = 1;
        source.Play();
        return this;
    }

    public void Stop()
    {
        if (IsPlaying() == false) return;
        source.Stop();
        SmoothSound(0, 1);
    }

    public void Resume()
    {
        source.Play();
    }

    void SmoothSound(float _value, float duration)
    {
        DOTween.To(() => source.volume, (x) => source.volume = x, _value, duration).SetUpdate(true);
    }

    public void SetMixerGroup(AudioMixerGroup _group)
    {
        source.outputAudioMixerGroup = _group;
    }


}
