using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatsControl : SerializedMonoBehaviour
{
    [SerializeField] CharacterStatsHandlerScriptable scriptable;
    [Header("debug")]
    [SerializeField] Dictionary<CharacterStatType, CharacterStat> runtimeStats = new Dictionary<CharacterStatType, CharacterStat>();

    private void Awake()
    {
        foreach (var keyValue in scriptable.stats) {
            runtimeStats[keyValue.Key] = new CharacterStat(keyValue.Value.Value);
        }
    }

    public CharacterStat GetStat(CharacterStatType _type)
    {
        if (runtimeStats.ContainsKey(_type) == false) return null;
        return runtimeStats[_type];
    }

    public float GetStatValue(CharacterStatType _type)
    {
        if (runtimeStats.ContainsKey(_type) == false) return 0;
        return runtimeStats[_type].Value;
    }

}

public class CharacterStat
{
    float value;
    public float Value { get { return value; } }
    float defaultValue;

    public CharacterStat(float _value)
    {
        defaultValue = _value;
        value = defaultValue;
    }

    public void AddValue(int _value)
    {
        value += _value;
    }

    public void OverrideValue(int _value)
    {
        value = _value;
        defaultValue = value;
    }

    public void RestoreValue()
    {
        value = defaultValue;
    }
}

public enum CharacterStatType
{
    Speed, Hp, Stamina, AttackDamg, AttackRange
}
