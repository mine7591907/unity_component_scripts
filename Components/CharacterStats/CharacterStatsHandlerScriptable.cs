using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
[CreateAssetMenu(fileName = "CharacterStat", menuName = GlobalScriptableString.Scriptable + "/" + "CharacterStat")]
public class CharacterStatsHandlerScriptable : SerializedScriptableObject
{
    public Dictionary<CharacterStatType, CharacterStatScriptable> stats = new Dictionary<CharacterStatType, CharacterStatScriptable>();

}
