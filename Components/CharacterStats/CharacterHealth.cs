using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{
    [SerializeField] CharacterStatsControl stats;
    [Range(0f, 1f)][SerializeField] float startHealthPercent = 1f;

    [Header("Debug")]
    [SerializeField] int curHealth;
    [SerializeField] int maxHealth;
    public int CurrentHealth => curHealth;
    public int MaxHealth => maxHealth;
    public event Action<int> onCurHealthChange;
    public event Action<float> onHealthPercentChange;
    public event Action onHealthZero;

    [SerializeField] bool isBelowZero = false;

    private void Start()
    {
        if (stats.GetStat(CharacterStatType.Hp) != null) Setup((int)stats.GetStat(CharacterStatType.Hp).Value);
    }

    void Setup(int _maxHealth)
    {
        isBelowZero = false;
        maxHealth = _maxHealth;
        curHealth = (int)((float)maxHealth * startHealthPercent);
        onCurHealthChange?.Invoke(curHealth);
        onHealthPercentChange?.Invoke(GetHealthPercentNormal());
    }

    public void SetHealth(int _health)
    {
        curHealth = _health;
        CheckHealth();
    }

    public void AddHealth(int _input)
    {
        curHealth += _input;
        CheckHealth();
    }

    void CheckHealth()
    {
        if (curHealth > maxHealth) curHealth = maxHealth;
        onCurHealthChange?.Invoke(curHealth);
        onHealthPercentChange?.Invoke(GetHealthPercentNormal());
        if (IsBelowZero() && isBelowZero == false)
        {
            isBelowZero = true;
            onHealthZero?.Invoke();
        }
    }

    public float GetHealthPercentNormal() => (float)curHealth / (float)maxHealth;

    bool IsBelowZero() => curHealth <= 0;


}
