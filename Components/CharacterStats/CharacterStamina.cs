using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CharacterStamina : MonoBehaviour
{
    public Action<float> onPercentChange;
    //[SerializeField] CharacterStat staminaStat;
    [SerializeField] CharacterStatsControl stats;
    [SerializeField] int restoreStaminaValue;
    [SerializeField] float restoreIterateTime;

    float value;
    float Value { get => value;
        set {
            this.value = value;
            onPercentChange?.Invoke((float)this.value / (float)maxValue);
        }
    }
    float maxValue;

    IEnumerator restoreStamina;

    private void Awake()
    {
        maxValue = stats.GetStat(CharacterStatType.Stamina).Value;
        Value = maxValue;
        restoreStamina = IERestoreStamina();
    }

    public void AddValue(int _value)
    {
        Value += _value;
        StartCoroutine(restoreStamina);
    }

    IEnumerator IERestoreStamina()
    {
        if (Value >= maxValue) yield break;
        yield return new WaitForSeconds(restoreIterateTime);
        Value += restoreStaminaValue;
        StartCoroutine(restoreStamina);
    }

    public float GetNormalizedValue()
    {
        return (float)Value / (float)maxValue;
    }

    public bool IsBelowOrAtZero()
    {
        return Value <= 0;
    }
}
