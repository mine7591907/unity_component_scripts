
using System;
using UnityEngine;


[Serializable]
public class CharacterStatScriptable 
{
    [SerializeField] float value;
    public float Value { get { return value; } }
}

