using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralCharacterDie : MonoBehaviour, ICharacterDie
{
    [SerializeField] CharacterStateManager characterStateManager;
    [SerializeField] CharacterHealth health;
    [SerializeField] Collider hitCollider;

    private void OnEnable()
    {
        health.onHealthZero += Die;
    }

    void OnDisable()
    {
        health.onHealthZero -= Die;
    }

    public void Die()
    {
        characterStateManager.SetState(State.Die);
        hitCollider.gameObject.ChangeLayer(GlobalString.layerUntouchable);
        //hitCollider.enabled = false;
        //characterStateManager.gameObject.SetActive(false);
    }

}
